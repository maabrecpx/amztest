# README #

System dependencies:

Python 2.7

rython-pip

phantomjs http://phantomjs.org/download.html

virtualenvwrapper* OPTIONAL

How to setup:
1 INSTALL DEPENDENCIES:

```
#!shell

cd path/to/project
pip install -r requirements.txt
```


2 SETUP RABBITMQ SERVER
check installation guide for your OS here: https://www.rabbitmq.com/download.html

with root run:

```
#!shell

rabbitmqctl add_user vbquser Vatb0xpw00d
rabbitmqctl set_user_tags vbquser administrator
rabbitmqctl set_permissions -p / vbquser ".*" ".*" ".*"
```

3 RUN DJANGO PRIOJECT
As this is a proof of concept it comes with some preloaded data, anyway if sqlite3 file is deleted you can manually init the DB


```
#!shell

./manage.py migrate
./manage.py shell
>>> from amazongc.models import Account
>>> a = Account(retailer="amazon.com", amazon_email="<your mail>", amazon_password="<your apss>", zipc="<your zip code>", phone="<your phone>", active=True)
>>> a.save()
```


Run project

```
#!shell

./manage.py runserver
```


4 RUN celery worker

```
#!shell

celery -A Tasks worker --app='backend.celery.worker' -c1 -Q worker -l info -Ofair


```