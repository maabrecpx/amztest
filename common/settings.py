from . import Settings
from kombu.common import Broadcast
import os
import django

DEBUG = False

# Celery and messaging settings.
CELERY_SETTINGS = Settings(
    BROKER_URL="amqp://vbquser:Vatb0xpw00d@127.0.0.1:5672/",
    CELERYD_HIJACK_ROOT_LOGGER=True,
    CELERY_ALWAYS_EAGER=DEBUG,
    CELERY_DEFAULT_QUEUE='vbdefault',
    CELERY_QUEUES=(Broadcast('broadcast'), ),
    CELERYD_POOL_RESTARTS=True,
)
NOTIFICATION_EXCHANGE = 'vbflow_notifications'
FLOW_EXCHANGE = 'vbflow'
WORKER_QUEUE = 'worker'

os.environ['DJANGO_SETTINGS_MODULE'] = 'amzn.settings'
django.setup()

