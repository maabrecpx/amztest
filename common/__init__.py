try:
    from django.utils.translation import ugettext_lazy as _
except ImportError:
    def _(s):
        return s


class Settings(object):
    """Object to store related settings."""

    keys = None
    arg_keys = {}
    defaults = {}

    def __init__(self, **kwargs):
        """Initialise.

        :param kwargs: Settings.

        """
        super(Settings, self).__init__()
        self.settings = self.defaults.copy()
        for k, v in kwargs.items():
            if self.keys is not None and k not in self.keys:
                raise TypeError('invalid argument {!r}'.format(k))
            self.settings[k] = v

    def __getattr__(self, key):
        """Support accessing settings as attributes."""
        try:
            return self.settings[key]
        except KeyError:
            raise AttributeError(key)

    def args(self, **keys):
        """Convert settings to a dictionary of arguments that can be passed to
        a function (e.g. a database initialisation function).

        :param keys: Dictionary keys to replace, use `None` to exclude a key.

            >>> from pprint import pprint
            >>> mysql_settings = Settings(host='localhost', port=3006,
            ...                           username='test', password='test')
            >>> pprint(mysql_settings.args(), width=68)
            {'host': 'localhost',
             'password': 'test',
             'port': 3006,
             'username': 'test'}
            >>> pprint(mysql_settings.args(username='user', password='pass'),
            ...        width=68)
            {'host': 'localhost', 'pass': 'test', 'port': 3006, 'user': 'test'}
            >>> pprint(mysql_settings.args(username=None, password=None),
            ...        width=68)
            {'host': 'localhost', 'port': 3006}

        """
        arg_keys = self.arg_keys.copy()
        arg_keys.update(keys)
        args = {}
        for key, value in self.settings.items():
            key = arg_keys.get(key, key)
            if key is not None:
                args[key] = value
        return args


class MongoDBSettings(Settings):
    """`Settings` adapted for use with `pymongo.MongoClient`."""
    arg_keys = {'db': None}