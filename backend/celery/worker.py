# -*- coding: utf-8 -*-
from __future__ import absolute_import
from celery import Celery

from common import settings

#celery -A Tasks worker --app='backend.celery.worker' -c1 -Q worker -l info -Ofair
celery = Celery('celery.worker')
celery_settings = settings.CELERY_SETTINGS.args()
celery_settings['CELERY_ALWAYS_EAGER'] = False
celery.conf.update(**celery_settings)

from backend.tasks.worker import process_messages