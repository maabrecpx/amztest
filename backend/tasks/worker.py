# -*- coding: utf-8 -*-
from __future__ import absolute_import
from celery.task import task
from celery import Task, registry
from backend.celery.worker import celery as worker_celery

from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.desired_capabilities import DesiredCapabilities
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.ui import WebDriverWait

from PIL import Image
import sys

import pyocr
import pyocr.builders
import cStringIO
import time
import urllib

@task
class ProcessMessageTask(Task):

    #TODO: Keep session alive, add a new task to re-login when new Account is actiavted... refactor redeem function, its ugly.

    tool = pyocr.get_available_tools()[0]

    def run(self, code, username, password, obj):
        n = 5
        #TODO: replace try-except blocks and use a good error and retry handling
        while n:
            try:
                r, msg, b, a = self.redeem(code, username, password)
                break
            except:
                try:
                    self.browser.quit()
                except:
                    pass
            n -= 1
            if not n:
                try:
                    r
                except:
                    r, msg, b, a = False, 'UNKNOWN ERROR', '', ''
        obj.status = 'SUCCESSFUL' if r else 'FAILED'
        obj.feedback = msg
        obj.balance_before = b
        obj.balance_after = a
        obj.save()
        try:
            self.browser.quit()
        except:
            pass
    
    #for local testing with real browser
    def get_browser(self):
        chrome_profile = webdriver.ChromeOptions()
        profile = {"plugins.plugins_disabled": ["Chrome PDF Viewer"]}
        chrome_profile.add_experimental_option("prefs", profile)
        chrome_profile.add_argument("-incognito")
        return webdriver.Remote(command_executor='http://127.0.0.1:4444/wd/hub',
                                desired_capabilities=chrome_profile.to_capabilities())

    def handle_captcha(self, code):
        time.sleep(5)
        img = self.browser.find_element_by_id('gc-captcha-image').get_attribute('src')
        file = cStringIO.StringIO(urllib.urlopen(img).read())
        img = Image.open(file)
        solution = self.tool.image_to_string(img)
        self.browser.find_element_by_id('gc-captcha-code-input').send_keys(solution)
        self.browser.find_element_by_id("gc-redemption-input").send_keys(code)
        self.browser.find_element_by_xpath('//*[@id="gc-redemption-apply-button"]').click()

    def redeem(self, code, username, password):
        #headless
        dcap = dict(DesiredCapabilities.PHANTOMJS)
        dcap["phantomjs.page.settings.userAgent"] = ( 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/55.0.2883.87 Safari/537.36' )
        self.browser = webdriver.PhantomJS(desired_capabilities=dcap,service_args=['--ssl-protocol=any','--ignore-ssl-errors=true'])
        self.browser.delete_all_cookies()
        self.browser.set_window_size(1920, 1080)
        #For local testing with real browser
        #self.browser = self.get_browser()
        
        print 'Started browser'
        self.browser.get("https://www.amazon.com/")
        login_page_link = self.browser.find_element_by_xpath("//div[@id='nav-flyout-ya-signin']/a").get_attribute("href")
        self.browser.get(login_page_link)
        
        print 'In Login page'
        self.browser.find_element_by_id("ap_email").send_keys(username)
        self.browser.find_element_by_id("ap_password").send_keys(password)
        time.sleep(1)
        self.browser.find_element_by_id("signInSubmit").send_keys(Keys.ENTER)
        
        if ' Your password is incorrect' in self.browser.page_source:
            print 'Login failed'
            return False, ' Your password is incorrect', 'n/a', 'n/a'

        print 'Logged in'

        self.browser.find_element_by_partial_link_text("Gift Cards").click()
        self.browser.find_element_by_xpath('.//a[@title="redeem a gift card"]').click()

        print 'GC redeem page'
        b = ''.join(self.browser.find_element_by_id('gc-current-balance').text)
        if 'Please enter the characters you see in the image below:' in self.browser.page_source:
            self.handle_captcha(code)
        else:
            self.browser.find_element_by_id("gc-redemption-input").send_keys(code)
            self.browser.find_element_by_xpath('//*[@id="gc-redemption-apply-button"]').click()
            if 'Please enter the characters you see in the image below:' in self.browser.page_source:
                self.handle_captcha(code)

        try:
            ah = self.browser.find_element_by_xpath('//h4[@class="a-alert-heading"]').text
        except:
            #TODO check if there are other messages here
            return False, 'Already applied {}'.format(self.browser.find_element_by_xpath('//div[@class="a-alert-content"]').text), b, b
        if "Invalid security verification" in ah:
            raise ValueError
        if "GC claim code is invalid" == ah:
            return False, 'GC claim code is invalid: {}'.format(self.browser.find_element_by_xpath('//div[@class="a-alert-content"]').text), b, b
        elif 'has been added to your Gift Card Balance' in ah:
            a = ''.join(self.browser.find_element_by_id('gc-current-balance').text)
            return True, ah, b, a
        else:
            return False, ah, b, b
        self.browser.quit()

process_messages = registry.tasks[ProcessMessageTask.name]
