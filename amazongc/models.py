from __future__ import unicode_literals

from django.db import models

#TODO: Models work but can be improved, some constraints missing.

class Account(models.Model):
    retailer = models.URLField(max_length=64, default='http://amazon.com', blank=False, null=False)
    amazon_email = models.EmailField(max_length=64, blank=False, null=False)
    amazon_password = models.CharField(max_length=64, blank=False, null=False)
    zipc = models.CharField(max_length=64, blank=False, null=False, verbose_name='zip code')
    phone = models.CharField(max_length=64, blank=False, null=False)
    active = models.BooleanField(default=False)
    
class Code(models.Model):
    code = models.CharField(max_length=64, blank=False, null=False)
    status = models.CharField(max_length=64, blank=False, null=False)
    date_used =  models.DateTimeField(auto_now=True)
    account_used = models.ForeignKey(Account, on_delete=models.CASCADE)
    balance_before = models.CharField(max_length=64, blank=True)
    balance_after = models.CharField(max_length=64, blank=True)
    feedback = models.TextField(null=True, blank=True)
    