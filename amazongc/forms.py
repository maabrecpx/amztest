# -*- coding: utf-8 -*-
from django import forms
from models import Account

class AccountForm(forms.ModelForm):
    class Meta:
        model = Account
        exclude = ('active', )
        widgets = {
            'amazon_password': forms.PasswordInput(),
            'zipc' : forms.TextInput(attrs={'type':'number'}),
            'phone': forms.TextInput(attrs={'type':'number'})
        }
    