# -*- coding: utf-8 -*-
from django.shortcuts import render, redirect
from backend.celery.worker import celery as worker_celery
from .forms import AccountForm
from models import Account, Code
from common import settings

#TODO: Enable admin and use @login_required to protect deployed app
def main_page(request):    
    active = Account.objects.get(active=True).amazon_email
    if request.method == 'POST':
        form = AccountForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect("list")
        else:
            return render(request, 'main.html', {'form':AccountForm(), 'active':active})
    else:
        return render(request, 'main.html', {'form':AccountForm(), 'active':active})

def account_list(request):
    if request.method == 'GET':
        if 'activate' in request.GET:
            #TODO: This is just ugly: there are better ways.
            _id = request.GET['activate']
            o = Account.objects.get(id=int(_id))
            
            if o:
                for x in Account.objects.all():
                    if x.active:
                        x.active = False
                        x.save()
                o.active = True
                o.save()
    active = Account.objects.get(active=True).amazon_email
    headers = [f.name for f in Account._meta.get_fields()]
    objs = Account.objects.all()  
    return render(request, 'list.html', {'objs':objs, 'h':headers, 'active':active})
    
def redeem(request):
    active = Account.objects.get(active=True)
    if request.method == 'POST':
        code = Code(code=request.POST['code'], 
                    status='PLEASE WAIT', 
                    account_used = active,
                    balance_before = '',
                    balance_after = '',
                    feedback="")
        code.save()
        worker_celery.send_task('backend.tasks.worker.ProcessMessageTask', 
                         ([request.POST['code'], active.amazon_email, active.amazon_password, code]), 
                         queue=settings.WORKER_QUEUE)
        return redirect("codes")
    return render(request, 'redeem.html', {'active':active.amazon_email})
    
def codes(request):
    active = Account.objects.get(active=True)
    headers = [f.name for f in Code._meta.get_fields()]
    objs = Code.objects.all()  
    return render(request, 'codes.html', {'objs':reversed([o for o in objs if o.account_used == active]), 'h':headers, 'active':active.amazon_email})