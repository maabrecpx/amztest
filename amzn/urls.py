"""amzn URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.10/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import url
from django.contrib import admin
import amazongc.views

urlpatterns = [
    url(r'^$', amazongc.views.main_page, name='main'),
    url(r'^list/$', amazongc.views.account_list, name='list'),
    url(r'^redeem/$', amazongc.views.redeem, name='redeem'),
    url(r'^codes/$', amazongc.views.codes, name='codes'),
]

admin.site.site_header = 'Amazon Gift Card Redeem'
